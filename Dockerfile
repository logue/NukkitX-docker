# Ubuntuをベースにする
FROM ubuntu:18.04

## 作成者
LABEL Logue <logue@hotmail.co.jp>

## 環境変数
ARG COUNTRY="ja"
ARG LANG="ja_JP.UTF-8"
ARG TIMEZONE="Asia/Tokyo"
ARG USE_ORACLE=false

## APTキャッシュを更新
RUN apt-get update
RUN apt-get upgrade -y

## 言語設定
ENV LANGUAGE=${LANG}:${COUNTRY}
ENV LC_ALL=${LANG}
ENV LC_CTYPE=${LANG}
RUN apt-get install -y language-pack-${COUNTRY}
RUN update-locale LANG=${LANG} LANGUAGE=${LANG}:${COUNTRY}

## 時刻設定
RUN ln -fs /usr/share/zoneinfo/${TIMEZONE} /etc/localtime

## ディスプレイ設定
ENV DISPLAY=":0.0"

## JAVAのインストール
RUN if [ "${USE_ORACLE}" = true ]; then \
    echo "oracle-java8-installer shared/accepted-oracle-license-v1-1 select true" | debconf-set-selections \
    echo "deb http://ppa.launchpad.net/webupd8team/java/ubuntu bionic main" > /etc/apt/sources.list.d/webupd8team-java-bionic.list \
    apt-key adv --keyserver keyserver.ubuntu.com --recv-keys EEA14886 \
    apt-get update && DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends oracle-java8-installer ; \
    else \
    apt-get install -y openjdk-8-jre-headless ; \
    fi

## 更新キャッシュをクリア
RUN apt-get clean && rm -rf /var/lib/apt/lists/*

## Javaの環境設定
#ENV JAVA_HOME="/usr/lib/jvm/java-8-oracle"
ENV JAVA_OPTS="-Djava.awt.headless=true"

## マウント先
VOLUME /srv/NukkitX

## 作業ディレクトリ
WORKDIR /srv/NukkitX

## ポート番号
EXPOSE 19132

## ビルドしたjupiterをルートに移動する
COPY NukkitX/target/nukkit-1.0-SNAPSHOT.jar /

## Jupiterを実行
CMD ["java", "-jar", "/nukkit-1.0-SNAPSHOT.jar"]

