# NukkitX Docker

これは、Minecraft PE向けのサーバープログラムであるNukkitの派生版のNukkitXのDocker Imageのリポジトリです。

<https://hub.docker.com/r/logue/nukkitx-docker/>

[![License: GPL v3](https://img.shields.io/badge/License-GPL%20v3-blue.svg)](LICENSE)
[![pipeline status](https://gitlab.com/logue/NukkitX-docker/badges/master/pipeline.svg)](https://gitlab.com/logue/NukkitX-docker/commits/master)
![Version](https://gitlab.com/logue/NukkitX-docker/raw/badges/version.png)
![Protocol](https://gitlab.com/logue/NukkitX-docker/raw/badges/protocol.png)

## このイメージの使い方

```sh
docker run -it -p 19132:19132/udp logue/NukkitX-docker [-e COUNTRY=ja -e LANG=ja_JP -e TIMEZONE=Asia/Tokyo -e USE_ORACLE=false]
```

## ボリューム

このサーバーのワーキングディレクトリは`/home/[username]/NukkitX`にマウントされ、設定ファイルや生成されたワールドなどのファイルシステムがマウントされます。

```sh
 docker run -v /home/[username]/NukkitX:/srv/NukkitX -p 19132:19132/udp logue/NukkitX-docker [-e COUNTRY=ja -e LANG=ja_JP -e TIMEZONE=Asia/Tokyo -e USE_ORACLE=false]
```

## Dockerfile

DockerfileはGitLabにホストされています. [GitHubのNukkitXのリポジトリ](https://github.com/JupiterDevelopmentTeam/NukkitX)はGitLabにミラーしています。 
[ミラーされたリポジトリ](https://gitlab.com/logue/NukkitX)は、Jupiterリポジトリ更新時に自動的に同期され、Dockerイメージとともにリビルドされます。

## タグ

プロトコル番号を明示的に指定したい場合は、以下のようにコマンドを実行します。

```sh
docker pull logue/NukkitX-docker:83
```

Minecraft PEのバージョンのように指定することもできます。

```sh
logue/NukkitX-docker:0.15.6
```

## 問題点

Dockerイメージに関する問題などがありましたら[GitLab issue](https://gitlab.com/logue/NukkitX-docker/issues)まで報告お願いします。